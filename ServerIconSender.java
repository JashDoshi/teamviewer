
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.zip.DeflaterOutputStream;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
public class ServerIconSender implements Runnable {
    OutputStream out;
    ImageIcon ic;
    BufferedImage img;
    ByteArrayOutputStream byteArrayOutputStream;
    DataOutputStream dos;
    ServerIconSender(BufferedImage img,OutputStream out){
        this.img=img;
        this.out=out;
        dos =  new DataOutputStream(out); 
    }
    public void run(){
        try {
            
            byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", byteArrayOutputStream);
            byte[] byteImage = byteArrayOutputStream.toByteArray();
            byteImage = compress(byteImage);
            System.out.println(byteImage.length+ "===== SERVER IS SENDING");
            dos.writeInt(byteImage.length);
            dos.write(byteImage,0,byteImage.length/2);
            dos.write(byteImage,byteImage.length/2,(byteImage.length-(byteImage.length/2)));
            
        } catch (IOException ex) {
            System.out.println(ex);
        }catch (Exception ex) {
            System.out.println(ex);
        }
        
    }
    @Override
    protected void finalize(){
            System.out.println("Called");
    }
    public byte[] compress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DeflaterOutputStream defl = new DeflaterOutputStream(out);
            defl.write(in);
            defl.flush();
            defl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(150);
            return null;
        }
    }
}
