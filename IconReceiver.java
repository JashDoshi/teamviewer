
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.zip.InflaterOutputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
public class IconReceiver extends Thread {
    InputStream in;
    
    ImageIcon ic;
    BufferedImage img;
    MainFrame frame;
    byte[] byteImage1;
    int size1;
    BlockingQueue<byte[]> blockingQueue;
    DataInputStream dis ;
    IconReceiver(InputStream in,MainFrame mf){
        this.in=in;
        this.frame=mf;
        blockingQueue=new ArrayBlockingQueue<byte[]>(2);
        size1=0;
        dis = new DataInputStream(in);
        System.out.println("Inside IconReceiver");
        start();
    }
    public void run(){
        while(true){
            try {
                size1=dis.readInt();
                byteImage1 = new byte[size1];
                
                dis.read(byteImage1,0,size1/2);
                dis.read(byteImage1,size1/2,(size1-size1/2));
                byteImage1 = decompress(byteImage1);
                img=ImageIO.read(new ByteArrayInputStream(byteImage1));
                System.out.println(img);
            } catch (Exception ex) {
                System.out.println(ex);
            } 
            if(img!=null){
                System.out.println("Done");
                frame.setIcon(new ImageIcon(img));

            }
        }
    }
    public byte[] decompress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InflaterOutputStream infl = new InflaterOutputStream(out);
            infl.write(in);
            infl.flush();
            infl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(150);
            return null;
        }
    }
}
