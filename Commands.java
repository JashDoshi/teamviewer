
import java.awt.Dimension;
import java.io.Serializable;
import javax.swing.ImageIcon;
class Commands implements Serializable{
    int x;
    int y;
    int keyCode;
    boolean click;
//        boolean dragged;
    Dimension dimension;
    Commands(int x,int y,boolean click,int keyCode,Dimension dimension){
            this.x=x;
            this.y=y;
            this.keyCode = keyCode;
            this.click = click;
            this.dimension = dimension;
//                this.dragged = dragged;
    }
    

    int getY(){
            return y;
    }
    int getX(){
            return x;
    }
    int getKey(){
        return keyCode;
    }
    boolean getClick(){
        return click;
    }
    Dimension getDimension(){
        return dimension;
    }
}