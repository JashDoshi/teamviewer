
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    int x;
    int y;
    int keyCode;
    boolean key = false;
    boolean click = false;
    boolean dragged = false;
    Image image;
    Image newimg;
    public MainFrame() {
        
        initComponents();
    }
    public void setIcon(ImageIcon ic){
//        image = ic.getImage();
//        newimg = image.getScaledInstance(1350,750,java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
//        ic=new ImageIcon(newimg);
        lblIcon.setIcon(ic);
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getKeyCode(){
        return keyCode;
    }
    public boolean getKey(){
        return key;
    }
    public boolean getClick(){
        return click;
    }
//    public boolean getDragged(){
//        return click;
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblIcon = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                formKeyTyped(evt);
            }
        });

        lblIcon.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lblIconMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                lblIconMouseMoved(evt);
            }
        });
        lblIcon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblIconMouseClicked(evt);
            }
        });
        lblIcon.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                lblIconKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                lblIconKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 790, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblIconKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lblIconKeyTyped
        // TODO add your handling code here:

    }//GEN-LAST:event_lblIconKeyTyped

    private void lblIconKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lblIconKeyReleased
        // TODO add your handling code here:
        //        
    }//GEN-LAST:event_lblIconKeyReleased

    private void lblIconMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIconMouseClicked
        // TODO add your handling code here:
        click = true;
    }//GEN-LAST:event_lblIconMouseClicked

    private void lblIconMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIconMouseMoved
        // TODO add your handling code here:
        this.x=evt.getX();
        this.y=evt.getY();
    }//GEN-LAST:event_lblIconMouseMoved

    private void lblIconMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIconMouseDragged
        // TODO add your handling code here:
        //        dragged = true;
    }//GEN-LAST:event_lblIconMouseDragged

    private void formKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyTyped
        // TODO add your handling code here:
        
    }//GEN-LAST:event_formKeyTyped

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        keyCode = evt.getKeyCode();
        key = true;
    }//GEN-LAST:event_formKeyPressed
    void setClickedFalse(){
        click = false;
    }
    void setKeyFalse(){
        key = false;
    }
//    void setDraggedFalse(){
//        dragged = false;
//    }
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblIcon;
    // End of variables declaration//GEN-END:variables
}
