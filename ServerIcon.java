
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.zip.InflaterOutputStream;

import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
public class ServerIcon implements Runnable {
    InputStream in;
    OutputStream out;
    ThreadPoolExecutor executor ;
    BufferedImage img;
    byte[] byteImage1;
    int size1;
    BlockingQueue<byte[]> blockingQueue;
    DataInputStream dis ;
    ServerIcon(OutputStream out,InputStream in,ThreadPoolExecutor executor ){
        this.executor = executor;
        this.out=out;
        this.in=in;
        blockingQueue=new ArrayBlockingQueue<byte[]>(2);
        dis = new DataInputStream(in);
        System.out.println("Inside server icon");
        
    }
    public void run(){
        int i=0;
        while(true){
            try {
                size1=dis.readInt();
                byteImage1 = new byte[size1];
                dis.read(byteImage1,0,size1/2);
                dis.read(byteImage1,size1/2,(size1-size1/2));
                byteImage1 = decompress(byteImage1);
                img=ImageIO.read(new ByteArrayInputStream(byteImage1));

                byteImage1=null;
                System.gc();
                if(img!=null){
                    System.out.println("img is not null");
                    executor.execute(new ServerIconSender(img,out));           
                }
                size1=0;
            }catch (Exception ex) {
                System.out.println(ex+" Exceptionnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
            }
        }
    }
    public byte[] decompress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InflaterOutputStream infl = new InflaterOutputStream(out);
            infl.write(in);
            infl.flush();
            infl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(150);
            return null;
        }
    }
}
