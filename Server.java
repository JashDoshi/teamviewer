import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import javax.swing.ImageIcon;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
public class Server {
    public static void main(String[] args){
        HashMap<String,Socket> hm;
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
		
        try{
            String receiverName = "";
            hm=new HashMap<>();
            ServerSocket server= new ServerSocket(8000);
            Socket client;
            InputStream inputStream;
            OutputStream outputStream;
            while(true){
                client = server.accept();
                System.out.println("client connected");
                inputStream=client.getInputStream();
                outputStream=client.getOutputStream();
                BufferedReader input=new BufferedReader(new InputStreamReader(inputStream));
                String name = input.readLine();
                hm.put(name,client);
                executor.execute(new ServerControler(client,input,executor,hm,name,inputStream,outputStream)); 
                System.out.println("Inserted in hash map");
                
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }
    @Override
    protected void finalize(){
        System.out.println("GC calledddddddddddddddddddddddddddddddddddddd");
    }
}