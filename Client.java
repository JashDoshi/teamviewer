
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.swing.JOptionPane;

class Client{
	public static void main(String[] args) {
		
            try{
                Robot robot=new Robot();
                Scanner scanner=new Scanner(System.in);
                ZipOutputStream zout;
                ZipInputStream zin;
                
                ObjectInputStream objectIn;
                ObjectOutputStream objectOut;
                
                
                
                String name = "";
                String receiver = "";
                
                Socket iconSocket=new Socket("localhost",8000);
                Socket commandSocket = new Socket("localhost",8001);
                
                OutputStream iconSocketOutputStream=iconSocket.getOutputStream();
                InputStream iconSocketInputStream=iconSocket.getInputStream();
                
                PrintWriter output=new PrintWriter(iconSocketOutputStream,true);
                PrintWriter output2=new PrintWriter(commandSocket.getOutputStream(),true);
                
                System.out.println("Enter your name : ");
                name = scanner.nextLine();
                output.println(name);
                output2.println(name);
                System.out.println("Do you want to send or receive (s/r): ");
                char ch = scanner.nextLine().charAt(0);
                output.println(ch+"");
                output2.println(ch+"");
                if(ch=='s' || ch=='S'){
                    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
                    
                    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
	
                    System.out.println("Enter receiver name : ");
                    receiver = scanner.nextLine();
                    output.println(receiver);
                    output2.println(receiver);
                    
                    objectIn = new ObjectInputStream(commandSocket.getInputStream());
                    new Thread(new Runnable(){
                        public void run(){
                            Commands c;
                            
                            int flagDrag = 1;
                            
                            while(true){
                                try{
                                    c=(Commands)objectIn.readObject();
                                    if(c!=null){
                                        int newX = ((int)c.getDimension().getWidth()/(int)dimension.getWidth())*c.getX();
                                        int newY = ((int)c.getDimension().getHeight()/(int)dimension.getHeight())*c.getY();
                                        System.out.println(newX+" : "+newY+" "+c.getClick()+" "+c.getDimension().getHeight());
                                        
//                                         robot.mouseMove(c.getX(),c.getY());
                                
                                        if(c.getClick()){
                                            robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                                            robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                                        }
                                        if(c.getKey()!= -1){
                                            robot.keyPress(c.getKey());
                                            robot.keyRelease(c.getKey());
                                        }
                                        
//                                        if(c.getDragged()){
//                                            robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
//                                            flagDrag = 1;
//                                        }else if(!c.getDragged() && flagDrag==1){
//                                            flagDrag = 0;
//                                            robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
//                                        }
                                    }
                                    Thread.sleep(50);
                                }catch(Exception e){}
                            }
                        }
                    }).start();
                    
                    executor.execute(new CaptureImage(iconSocketOutputStream,executor));
                    

                    
                }else if(ch=='r' || ch=='R') {
                  
                    objectOut = new ObjectOutputStream(commandSocket.getOutputStream());
                    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
                    
                    System.out.println("Inside receiver");
                    MainFrame mf=new MainFrame();
                    mf.setVisible(true);
                    IconReceiver iconReceiver = new IconReceiver(iconSocketInputStream,mf);
                    
                    new Thread(new Runnable(){
//                        Commands commands=new Commands(x,y);
                        public void run(){
                            boolean isClicked = false;
                            boolean isKey = false;
                            int keyCode=-1;
                            try {
                                Thread.sleep(15000);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            while(true){
                                try {
                                    isClicked = mf.getClick();
                                    isKey = mf.getKey();
                                    if(isKey){
                                        keyCode = mf.getKeyCode();
                                        mf.setKeyFalse();
                                    }else
                                        keyCode = -1;
                                        
                                    mf.setClickedFalse();
                                    
//                                    mf.setDraggedFalse();
                                        objectOut.writeObject(new Commands(mf.getX(),mf.getY(),isClicked,keyCode,dimension));
                                    Thread.sleep(20);
                                    System.out.println("Send : "+mf.getX()+"  :  "+mf.getY()+" : "+keyCode + " : "+isClicked);
                                    
                                } catch (IOException ex) {
                                    System.out.println(ex);
                                } catch(Exception e){
                                    System.out.println(e);
                                }
                                
                            }
                        }
                    }).start();
                }
            }catch(Exception e){
                    System.out.println(e);
            }
	}
}