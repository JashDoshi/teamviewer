
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.zip.DeflaterOutputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
public class IconSender implements Runnable{
    OutputStream out;
    ImageIcon ic;
    BufferedImage image;
    ByteArrayOutputStream byteArrayOutputStream;
    DataOutputStream dos;
    IconSender(OutputStream out,BufferedImage image){
        this.out=out;
        this.image=image;
        System.out.println("Inside IconSender");
        dos =  new DataOutputStream(out); 
    }
    public void run(){       
        try {
            image = Scalr.resize(image,Method.ULTRA_QUALITY,Mode.AUTOMATIC,1200, 900,Scalr.OP_ANTIALIAS); 
            byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", byteArrayOutputStream);
            byte[] byteImage = byteArrayOutputStream.toByteArray();
            byteImage = compress(byteImage);
            System.out.println(byteImage.length);
            
            dos.writeInt(byteImage.length);
            dos.write(byteImage,0,byteImage.length/2);
            dos.write(byteImage,byteImage.length/2,(byteImage.length-(byteImage.length/2)));
            
            Thread.sleep(20);
            System.out.println("Image send");
            byteImage=null;
        } catch (IOException ex) {
            System.out.println(ex);
        }catch (Exception ex) {
            System.out.println(ex);
        }
    }
    @Override
    protected void finalize(){
            System.out.println("Called");
    }
    public byte[] compress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DeflaterOutputStream defl = new DeflaterOutputStream(out);
            defl.write(in);
            defl.flush();
            defl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(150);
            return null;
        }
    }
}
