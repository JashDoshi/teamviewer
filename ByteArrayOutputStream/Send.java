import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;
import javax.imageio.ImageIO;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.stream.ImageOutputStream;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import java.util.Arrays;
public class Send {

    public static void main(String[] args) throws Exception {
        Socket socket = new Socket("localhost", 13085);
        
        Robot robot=new Robot();
        Rectangle screenRect=new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());   
        BufferedImage image;
        ByteArrayOutputStream byteArrayOutputStream;
        // ZipOutputStream zout = new ZipOutputStream(socket.getOutputStream()); 
        // zout.putNextEntry(new ZipEntry("bef"));       
        OutputStream outputStream = socket.getOutputStream();
        while(true){
            image = robot.createScreenCapture(screenRect);
            // ImageIO.write(image,"jpg",new File("img2.jpg"));
            image = Scalr.resize(image,Method.ULTRA_QUALITY,Mode.AUTOMATIC,1200, 900,Scalr.OP_ANTIALIAS); 
            byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", byteArrayOutputStream);
            byte[] byteImage = byteArrayOutputStream.toByteArray();
            System.out.println(byteImage.length);

            outputStream.write(Arrays.copyOfRange(byteImage,0,byteImage.length/2));
            outputStream.write(Arrays.copyOfRange(byteImage,byteImage.length/2,byteImage.length-1));
            
            System.out.println("Flushed: " + System.currentTimeMillis());

            Thread.sleep(150);
            System.out.println("Closing: " + System.currentTimeMillis());
        }
        // socket.close();
    }
}


