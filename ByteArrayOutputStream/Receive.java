import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.SocketTimeoutException;
import java.util.Scanner;
import javax.imageio.ImageIO;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
public class Receive extends JFrame{
    JLabel label;
    JPanel panel;
    Receive(){
        
        setSize(500,500);
        setLayout(new FlowLayout());
        panel=new JPanel();
        add(panel);
        label=new JLabel();
        panel.add(label);
    }
    void addLabel(ImageIcon ic){
        setVisible(true);
        label.setIcon(ic);
        panel.updateUI();
    }
    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(13085);
        Receive obj=new Receive();
        Socket socket = serverSocket.accept();
        // ZipInputStream zin = new ZipInputStream();
        // zin.getNextEntry();
        InputStream inputStream = socket.getInputStream();
        
        byte[] byteImg;
        int size1=0;
        int size2=0;
        BufferedImage image;
        while(true){
            byte[] byteImage1 = new byte[100000];
            byte[] byteImage2 = new byte[100000];
            System.out.println("Reading: " + System.currentTimeMillis());
            Thread.sleep(950);
            size1=inputStream.read(byteImage1);
            Thread.sleep(950);
            size2=inputStream.read(byteImage2);
            byteImg=new byte[size1+size2];
            System.arraycopy(byteImage1,0,byteImg,0,size1);
            System.arraycopy(byteImage2,0,byteImg,size1,size2);
            
            image=ImageIO.read(new ByteArrayInputStream(byteImg));
            // image=Scalr.resize(image,Method.ULTRA_QUALITY,Mode.AUTOMATIC,1033, 768,Scalr.OP_ANTIALIAS);
            obj.addLabel(new ImageIcon(image));
            System.out.println("Received ");    
        }
        
        // ImageIO.write(image, "jpg", new File("img.jpg"));

        // serverSocket.close();
    }

}
