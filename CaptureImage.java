
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ThreadPoolExecutor;
import javax.swing.ImageIcon;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vijay
 */
public class CaptureImage implements Runnable {
    Robot robot;
    Rectangle screenRect;
    ImageIcon ic;
    BufferedImage screenFullImage ;
    ThreadPoolExecutor executor ;
    OutputStream out;
    CaptureImage(OutputStream out,ThreadPoolExecutor executor ){
        try {
            this.executor=executor;
            this.out=out;
            robot = new Robot();
        } catch (AWTException ex) {}
        screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
    }
    public void run(){
        while(true){
            screenFullImage = robot.createScreenCapture(screenRect); 
          
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
            executor.execute(new IconSender(out,screenFullImage));
            System.gc();
        }
    }
    @Override
    protected void finalize(){
        System.out.println("Calleddddddddddddddd");
    }
}
